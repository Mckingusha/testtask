
package task;


public class Operation {
    private boolean valid;
    private int type = 0;
    private int a;
    private int b;
    private String result;
    private String errorMessage;
    
    public Operation(boolean valid){
        this.valid = valid;
    } 
    public Operation(boolean valid, int a, int b, int type){
        this.valid = valid;
        this.a = a;
        this.b = b;
        this.type = type;
    }
    public Operation(String errorMessage){
        this.errorMessage = errorMessage;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    
}
