package task;
import javax.xml.soap.*;
import javax.xml.namespace.*;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
public class SOAPClientSAAJ {

    // SAAJ - SOAP Client Testing
    
    
    

    private static void createSoapEnvelope(SOAPMessage soapMessage, Operation oper) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();


        SOAPEnvelope envelope = soapPart.getEnvelope();

        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = null;
        switch(oper.getType()){
            case 0:
                soapBodyElem = soapBody.addChildElement("Add","","http://tempuri.org/");
                break;
            case 1:
                soapBodyElem = soapBody.addChildElement("Divide","","http://tempuri.org/");
                break;
            case 2:
                soapBodyElem = soapBody.addChildElement("Multiply","","http://tempuri.org/");
                break;
            case 3:
                soapBodyElem = soapBody.addChildElement("Subtract","","http://tempuri.org/");
                break;
        }
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("intA");
        soapBodyElem1.addTextNode(String.valueOf(oper.getA()));
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("intB");
        soapBodyElem2.addTextNode(String.valueOf(oper.getB()));

    
    }

    public static String callSoapWebService(String soapEndpointUrl, String soapAction, Operation oper) {
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction, oper), soapEndpointUrl);

            SOAPBody soapResponseBody = soapResponse.getSOAPBody();
            NodeList nodes = null;

            switch(oper.getType()){
            case 0:
                nodes = soapResponseBody.getElementsByTagName("AddResult");
                break;
            case 1:
                nodes = soapResponseBody.getElementsByTagName("DivideResult");
                break;
            case 2:
                nodes = soapResponseBody.getElementsByTagName("MultiplyResult");
                break;
            case 3:
                nodes = soapResponseBody.getElementsByTagName("SubtractResult");
                break;
            }
            Node node = nodes.item(0);
            String result = node.getTextContent();
            
            System.out.println("Response SOAP Message:");
            soapResponse.writeTo(System.out);
            System.out.println();

            soapConnection.close();
            return(result);
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }
    } 

    private static SOAPMessage createSOAPRequest(String soapAction, Operation oper) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage, oper);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }

}