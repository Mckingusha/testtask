package task;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OperationController {

    private Operation oper;
    private String soapEndpointUrl = "http://www.dneonline.com/calculator.asmx";
    private String soapAction = "";

    @RequestMapping("/add")
    public Operation add(@RequestParam(value="a", required=false) String a,
                            @RequestParam(value="b", required=false) String b) {
        oper = myValidate(a,b,0);
        if (!oper.isValid()){
            oper.setErrorMessage("Invalid Operands");
            return oper;
        }
        soapAction = "http://tempuri.org/Add";
        oper.setResult(SOAPClientSAAJ.callSoapWebService(soapEndpointUrl, soapAction, oper));
        return oper;
    }
    
    @RequestMapping("/divide")
    public Operation divide(@RequestParam(value="a", required=false) String a,
                            @RequestParam(value="b", required=false) String b) {
        oper = myValidate(a,b,1);
        if (!oper.isValid() || oper.getB() == 0){
            oper.setValid(false);
            oper.setErrorMessage("Invalid Operands");
            return oper;
        }
        soapAction = "http://tempuri.org/Divide";
        oper.setResult(SOAPClientSAAJ.callSoapWebService(soapEndpointUrl, soapAction, oper));
        return oper;
    }

    @RequestMapping("/multiply")
    public Operation multiply(@RequestParam(value="a", required=false) String a,
                            @RequestParam(value="b", required=false) String b){
        oper = myValidate(a,b,2);
        if (!oper.isValid()){
            oper.setErrorMessage("Invalid Operands");
            return oper;
        }
        soapAction = "http://tempuri.org/Multiply";
        oper.setResult(SOAPClientSAAJ.callSoapWebService(soapEndpointUrl, soapAction, oper));
        return oper;
    }

    @RequestMapping("/subtract")
    public Operation subtract(@RequestParam(value="a", required=false) String a,
                            @RequestParam(value="b", required=false) String b) {
        oper = myValidate(a,b,3);
        if (!oper.isValid()){
            oper.setErrorMessage("Invalid Operands");
            return oper;
        }
        soapAction = "http://tempuri.org/Subtract";
        oper.setResult(SOAPClientSAAJ.callSoapWebService(soapEndpointUrl, soapAction, oper));
        return oper;
    } 
    /*
    @RequestMapping("/error")
    public Operation err() {
        return new Operation("Invalid URL");
    }
    **/
    public Operation myValidate(String a, String b, int type){
        try{
            int first = Integer.valueOf(a);
            int second = Integer.valueOf(b);
            return new Operation(true, first, second, type);
        }
        catch(Exception ex){
            ex.printStackTrace();
            return new Operation(false);
        }
    }
}